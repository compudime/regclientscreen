// confing lib
//const angular = require('angular');
//const bootstrap = require('bootstrap');

const CilentScreen = angular.module("cilent",[]);
//-------------------------------------------
CilentScreen.service('smoothScroll', function(){
    
    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        let startY = currentYPosition();
        let stopY = elmYPosition(eID);
        let distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        let speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        let step = Math.round(distance / 15);
        let leapY = stopY > startY ? startY + step : startY - step;
        let timer = 0;
        if (stopY > startY) {
            for ( let i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( let i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            let elm = document.getElementById(eID);
            let y = elm.offsetTop;
            let node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }
    };   
});
//-------------------------------------------
CilentScreen.controller('app',['$scope','$timeout','$http','$location','$log','smoothScroll',function($scope ,$timeout,$http,$location, $log,smoothScroll){
    let DataLength= null;
    let DataArray = [];
    var oldData = null;
    var currentData = null;
    http();
    //------------------------------------------------------http module starts--------------------------------------------------------
    function http(){
        $timeout(()=>{
            $http({
                url: ('http://localhost:4300'),
                method: "GET",
                data: []}).then((jdata)=>{
                currentData = jdata.data; 
                $scope.$emit('dataReceived');
            }).catch((error)=>{  
                $log.error("Error:",error);
                currentData = error.status;
                $scope.$emit('dataError');
            });
        },500);
    }

    $scope.$on('dataError',()=>{
        if(currentData == 404){
            $log.info('no new file yet'); 
        }
        if(currentData == -1){
            $log.info('server is not running');
        }
        if($scope.total == null)
            {setPageData(null);}
        http();
    });

    $scope.$on('dataReceived',()=>{
        switch (currentData) {
            case "":
                $log.info("no files");
                if($scope.total == null)
                    {setPageData(null);}
                break;
            default:
                //$log.log(currentData);
                if(oldData !== currentData){
                    setPageData(currentData);
                    oldData = currentData;
                }
        }
        http();   
    });

    //----------------------------------------------http module ends-----------------------------------------------------------------------

    function setPageData(jsonData){
        if(jsonData != null){
            $scope.items = jsonData.totalcount;
            $scope.total = jsonData.total;
        }
        else{
            $scope.items = "0";
            $scope.total = "0";
        }
        let obj = {qty:"0",descrip:"",price:"",total:""};
        if(jsonData!== null && jsonData != null){
            DataArray = jsonData.items.slice(0);
            DataLength = DataArray.length;
            for(let i=0;i<20 - jsonData.items.length;i++){
                DataArray.push(obj);
            }
        }
        else{
            for(let i=0;i<100;i++){
                DataArray.push(obj);
            }
        }
        $scope.table = DataArray;
        //$log.log(DataArray);
        
        $scope.setColor = (qty)=>{
            if(qty === "0")return {color:'transparent'};
        };
        $scope.showData = ()=>{return false;};
        //create logic for reg header
        $scope.asSell = function(){return {color:'transparent'};};
        //scroll to location
        angular.element(document).ready(()=>{
            let lastElement = document.getElementById('row'+(DataLength - 1).toString());
            let lastElementOffSet = lastElement.offsetTop;
            let lastElementSize = lastElement.offsetHeight;
            let topBarSize = document.getElementById('topBar').offsetHeight;
            let botBarSize = document.getElementById('botBar').offsetHeight;
            let windowSize = window.innerHeight - botBarSize;
            let tableSize = (windowSize - topBarSize);
            let numberOfRowsOutOfView = (lastElementOffSet - tableSize)/lastElementSize;
            $log.log("size:"+lastElementSize,"win size:"+tableSize,"bottom:"+lastElementOffSet);
            if(DataLength != null && lastElementOffSet > tableSize){ 
                    $log.log(numberOfRowsOutOfView);
                    scroll('row'+Math.round(numberOfRowsOutOfView));
            }
        });
    }

    //scroll to function
    function scroll(eID){
        // set the location.hash to the id of
        // the element you wish to scroll to.
        $location.hash('row'+(DataLength - 1).toString());

        // call $anchorScroll()
        smoothScroll.scrollTo(eID);
    }
}]);

